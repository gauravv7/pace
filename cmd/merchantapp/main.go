package main

import (
	"github.com/gauravv7/merchantapp/api/v1/router"
	"github.com/gauravv7/merchantapp/internal/db"
	"log"
)

func main() {
	db.CreateMongoDbConnection()
	err := db.MgClient.InitIndexes(db.DB_NAME, db.COL_MERCHANTS)
	if err != nil {//handle errors
		log.Fatalf("error while creating indexes: %s", err)
	}
	err = db.MgClient.InitIndexes(db.DB_NAME, db.COL_TEAM_MEMBERS)
	if err != nil {//handle errors
		log.Fatalf("error while creating indexes: %s", err)
	}
	router.ServeRouter()
}

