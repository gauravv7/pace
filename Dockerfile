
FROM golang:1.17-alpine3.15 AS builder

ENV GO111MODULE=on

WORKDIR /root

COPY . .

ENV TZ America/Chicago

#dependencies
RUN apk add build-base
RUN echo $GOPATH
RUN unset GOPATH
RUN echo $GOPATH
RUN ls -al
RUN pwd
RUN go mod tidy

WORKDIR ./cmd/merchantapp/

RUN GOOS=linux go build -a -installsuffix cgo -o app .


#Second stage for a smaller image
FROM alpine:3.15 as dev
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /root/cmd/merchantapp/app /root/
EXPOSE 8080/udp
EXPOSE 8080/tcp
RUN pwd
RUN ls -al
CMD ["./app"]