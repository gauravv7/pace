package utils

import (
	"encoding/json"
	"github.com/gauravv7/merchantapp/internal/models"
	"log"
	"net/http"
	"strconv"
)

func WriteResponseAsJson(w http.ResponseWriter, data interface{}, statusCode int) {
	jData, err := ConvertToJson(data)
	if err != nil {
		// handle error
		log.Println("error while converting to json", err)
		response := models.Response{
			Success:  false,
			Data:     nil,
			ErrorMsg: err.Error(),
		}
		jData, _ = ConvertToJson(response)
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	w.Write(jData)
}

func ConvertToJson(data interface{}) ([]byte, error) {
	jData, err := json.Marshal(data)
	return jData, err
}

func GetInt64(data string) int64 {
	i, err := strconv.ParseInt(data, 10, 64)
	if err != nil {
		log.Fatalf("cannot convert to int64: %s", err)
	}
	return i
}