package db

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"time"
)

var MgClient *Mongo

const DB_NAME string = "pacedb"
const COL_MERCHANTS string = "merchants"
const COL_TEAM_MEMBERS string = "team_members"
const CONN_STRING string = "mongodb://root:example@mymng:27017/"

//NewMongoClient is a constructor for a mongo client, it has to connect outside
func NewMongoClient(mongoConn string) (*Mongo, error) {
	client, err := mongo.NewClient(options.Client().ApplyURI(mongoConn))
	if err != nil {
		return nil, err
	}
	mg := Mongo{
		client,
	}
	return &mg, nil
}

//PingServer pings the server and check if its found
func (m *Mongo) PingServer() error {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	err := m.Ping(ctx, readpref.Primary())

	return err
}

//ConnectClient connects the client instance
func (m *Mongo) ConnectClient() error {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	err := m.Connect(ctx)

	return err
}

func CreateMongoDbConnection() {
	log.Println("Creating New Mongodb client")
	c, err := NewMongoClient(CONN_STRING)
	if err != nil {
		log.Fatal("Error while creating New Mongodb client", err)
	}

	log.Println("Pinging Mongodb")
	err = c.PingServer()
	if err != nil {
		log.Println("Error while pinging Mongodb", err)

		log.Println("Establishing Mongodb Connection")
		err = c.ConnectClient()
		if err != nil {
			log.Fatal("Error while Establishing Mongodb Connection", err)
		}
		MgClient = c
	}
}
