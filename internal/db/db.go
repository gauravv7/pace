package db

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"time"
)


type Mongo struct {
	*mongo.Client
}

//MongoCRUD is an interface to abstract and test the packages
type MongoCRUD interface {
	InitIndexes(database, collection string) error
	Aggregate(database, collection string, selector bson.M, options options.AggregateOptions, output interface{}) error
	FindMany(database, collection string, selector bson.D, options options.FindOptions, output interface{}) error
	FindOne(database, collection string, selector bson.D, options options.FindOptions, output interface{}) error
	InsertOne(database, collection string, options options.InsertOneOptions, insert interface{}) error
	InsertMany(database, collection string, options options.InsertManyOptions, insert interface{}) error
	Upsert(database, collection string, selector bson.D, options options.UpdateOptions, update interface{}) error
	Update(database, collection string, selector bson.D, options options.UpdateOptions, update interface{}) error
	Delete(database, collection string, options options.DeleteOptions, selector bson.D) error
}


func (m *Mongo) InsertOne(database, collection string, options *options.InsertOneOptions, insert interface{}) error {
	coll := m.Database(database).Collection(collection)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := coll.InsertOne(ctx, insert, options)
	//the object from InsertOne contains the _id of the inserted doc in case is needed
	return err
}

func (m *Mongo) InitIndexes(database, collection string) error {
	coll := m.Database(database).Collection(collection)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	var indexes []mongo.IndexModel
	if collection == COL_MERCHANTS {
		log.Printf("creating indexes on %s collections, unique index on email, account_number, account_name", collection)
		indexes = []mongo.IndexModel{
			{
				Keys:    bson.D{{Key: "email", Value: 1}},
				Options: options.Index().SetUnique(true).SetName("mer_email_unique"),
			},
			{
				Keys:    bson.D{{Key: "account_number", Value: 1}},
				Options: options.Index().SetUnique(true).SetName("mer_anum_unique"),
			},
			{
				Keys:    bson.D{{Key: "account_name", Value: 1}},
				Options: options.Index().SetUnique(true).SetName("mer_aname_unique"),
			},
		}
	} else {
		log.Printf("creating indexes on %s collections, unique index on name, email", collection)
		indexes = []mongo.IndexModel{
			{
				Keys:    bson.D{{Key: "email", Value: 1}},
				Options: options.Index().SetUnique(true).SetName("tm_email_unique"),
			},
			{
				Keys:    bson.D{{Key: "name", Value: 1}},
				Options: options.Index().SetUnique(true).SetName("tm_name_unique"),
			},
		}
	}

	_, err := coll.Indexes().CreateMany(
		ctx,
		indexes,
	)
	return err
}

//InsertMany inserts the documents specified in the insert, it has to be an array
func (m *Mongo) Aggregate(database, collection string, selected []bson.M, options *options.AggregateOptions, output interface{}) error {
	coll := m.Database(database).Collection(collection)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	cur, err := coll.Aggregate(ctx, selected, options)
	if err == mongo.ErrNoDocuments {
		//handle the no record found
	} else if err != nil {
		return err
	}
	err = cur.All(ctx, output)
	if err != nil {//handle errors
	}
	return err
}

//InsertMany inserts the documents specified in the insert, it has to be an array
func (m *Mongo) InsertMany(database, collection string, options *options.InsertManyOptions, insert interface{}) error {
	coll := m.Database(database).Collection(collection)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := coll.InsertMany(ctx, insert.([]interface{}), options)
	if err != nil {//handle errors
	}
	return err
}

//FindMany uses an abstraction of the find function with a cursor
func (m *Mongo) FindMany(database, collection string, selector bson.D, opts *options.FindOptions, output interface{}) error {
	coll := m.Database(database).Collection(collection)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	cur, err := coll.Find(ctx, selector, opts)
	if err == mongo.ErrNoDocuments {
		//handle the no record found
	} else if err != nil {
		return err
	}
	err = cur.All(ctx, output)
	if err != nil {//handle errors
	}
	return err
}

//FindMany uses an abstraction of the find function with a cursor
func (m *Mongo) FindOne(database, collection string, selector bson.D, opts *options.FindOptions, output interface{}) error {
	coll := m.Database(database).Collection(collection)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err := coll.FindOne(ctx, selector).Decode(output)
	if err == mongo.ErrNoDocuments {
		//handle the no record found
	} else if err != nil {
		//handle errors
	}
	return err
}

func (m *Mongo) UpdateOne(database, collection string, selector bson.D, opts *options.UpdateOptions, update interface{}) error {
	coll := m.Database(database).Collection(collection)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := coll.UpdateOne(ctx, selector, update, opts)
	//the update object contains useful information that you may want to use
	if err != nil {
		return err
	}
	return nil
}


func (m *Mongo) UpdateMany(database, collection string, selector bson.D, opts *options.UpdateOptions, update interface{}) error {
	coll := m.Database(database).Collection(collection)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := coll.UpdateMany(ctx, selector, update, opts)
	//the update object contains useful information that you may want to use
	if err != nil {
		return err
	}
	return nil
}

func (m *Mongo) DeleteOne(database, collection string, options *options.DeleteOptions, selector bson.D) error {
	coll := m.Database(database).Collection(collection)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := coll.DeleteOne(ctx, selector, options)
	if err != nil {
		//handle error
	}
	return err
}

func (m *Mongo) DeleteMany(database, collection string, options *options.DeleteOptions, selector bson.D) error {
	coll := m.Database(database).Collection(collection)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := coll.DeleteMany(ctx, selector, options)
	//The return object from Delete contains the count of the elements deleted
	if err != nil {
		//handle error
	}
	return err
}