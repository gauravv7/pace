package db

//Extends crud functionality and adds client functions
type DBConnection interface {
	PingServer() error
	ConnectClient() error
	MongoCRUD
}
