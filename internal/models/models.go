package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type MerchantAccount struct {
	ID            primitive.ObjectID `bson:"_id" json:"id" omitempty`
	AccountName   string             `bson:"account_name" json:"account_name"`
	AccountNumber int                `bson:"account_number" json:"account_number"`
	Email         string             `bson:"email" json:"email"`
	CreatedAt     time.Time          `bson:"created_at" json:"created_at"`
	UpdatedAt     time.Time          `bson:"updated_at" json:"Updated_at"`
}

type UpdateMerchantAccount struct {
	AccountName   string             `bson:"account_name" json:"account_name"`
}

type UpdateTeamMember struct {
	Name   string             `bson:"name" json:"name"`
}

type TeamMemberRequest struct {
	ID                    primitive.ObjectID `bson:"_id" json:"id" omitempty`
	Name                  string             `bson:"name" json:"name"`
	MerchantAccountNumber int             `bson:"merchant_account_number" json:"merchant_account_number"`
	Email                 string             `bson:"email" json:"email"`
	CreatedAt             time.Time          `bson:"created_at" json:"created_at"`
	UpdatedAt             time.Time          `bson:"updated_at" json:"updated_at"`
}

type TeamMember struct {
	ID                    primitive.ObjectID `bson:"_id" json:"id" omitempty`
	Name                  string             `bson:"name" json:"name"`
	MerchantAccount       MerchantAccount    `bson:"merchant" json:"merchant"`
	Email                 string             `bson:"email" json:"email"`
	CreatedAt             time.Time          `bson:"created_at" json:"created_at"`
	UpdatedAt             time.Time          `bson:"updated_at" json:"updated_at"`
}

type Response struct {
	Success		bool		`json:"success"`
	Data 		interface{}	`json:"data"`
	ErrorMsg	string		`json:"error_msg"`
}