package handlers

import (
	"encoding/json"
	"github.com/gauravv7/merchantapp/internal/db"
	"github.com/gauravv7/merchantapp/internal/models"
	"github.com/gauravv7/merchantapp/internal/utils"
	"github.com/go-chi/chi"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net/http"
	"strings"
)

func MerchantRoutes( /* any dependency injection comes here*/ ) *chi.Mux {
	r := chi.NewRouter()


	r.Get("/all", GetAllMerchants())              //Implementation of routes from merchantHandler.go
	r.Get("/email/{email}", GetMerchantByEmail()) //Implementation of routes from merchantHandler.go
	r.Post("/create", CreateMerchant())
	r.Delete("/email/{email}", DeleteMerchantByEmail())
	r.Put("/email/{email}", UpdateMerchant())

	return r
}

//GetAllMerchants get all merchants
func GetAllMerchants() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		curPage := r.URL.Query().Get("page")
		if strings.Trim(curPage, " ") == "" {
			curPage = "1"
		}

		count := r.URL.Query().Get("count")
		if strings.Trim(count, " ") == "" {
			count = "10"
		}

		merchantAccs := make([]models.MerchantAccount, 1)
		findOptions := options.Find()
		findOptions.SetSkip((utils.GetInt64(curPage)-1)*utils.GetInt64(count))
		findOptions.SetLimit(utils.GetInt64(count))

		err := db.MgClient.FindMany(db.DB_NAME, db.COL_MERCHANTS, bson.D{{}}, findOptions, &merchantAccs)
		if err != nil {
			log.Println("error while fetching data", err)
			utils.WriteResponseAsJson(w, models.Response{Success: false, Data: nil, ErrorMsg: err.Error()}, http.StatusInternalServerError)
			return
		}
		utils.WriteResponseAsJson(w, merchantAccs, http.StatusOK)
	}
}

//GetMerchantByEmail get merchant by email
func GetMerchantByEmail() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		merchantAccs := make([]models.MerchantAccount, 1)

		email := chi.URLParam(r, "email")
		if len(strings.Trim(email, " ")) < 1 {
			log.Printf("invalid input email: %s ", email)
			utils.WriteResponseAsJson(w, models.Response{Success: false, Data: nil, ErrorMsg: "invalid input"}, http.StatusInternalServerError)
			return
		}
		err := db.MgClient.FindMany(db.DB_NAME, db.COL_MERCHANTS, bson.D{{"email", email}}, options.Find(), &merchantAccs)
		if err != nil {
			log.Printf("error while finding merchants by email: %s ", err)
			utils.WriteResponseAsJson(w, models.Response{Success: false, Data: nil, ErrorMsg: err.Error()}, http.StatusInternalServerError)
			return
		}
		utils.WriteResponseAsJson(w, merchantAccs, http.StatusOK)
	}
}

//CreateMerchant create a merchant
func CreateMerchant() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var merchantAccount models.MerchantAccount

		// Try to decode the request body into the struct. If there is an error,
		// respond to the client with the error message and a 400 status code.
		err := json.NewDecoder(r.Body).Decode(&merchantAccount)
		if err != nil {
			log.Printf("error while parsing request: %s ", err)
			utils.WriteResponseAsJson(w, models.Response{Success: false, Data: nil, ErrorMsg: "bad request"}, http.StatusBadRequest)
			return
		}

		merchantAccount.ID = primitive.NewObjectID()
		err = db.MgClient.InsertOne(db.DB_NAME, db.COL_MERCHANTS, nil, merchantAccount)
		if err != nil {
			log.Printf("error while inserting merchant: %s ", err)
			utils.WriteResponseAsJson(w, models.Response{Success: false, Data: nil, ErrorMsg: err.Error()}, http.StatusInternalServerError)
			return
		}
		utils.WriteResponseAsJson(w, models.Response{Success: true, Data: "created", ErrorMsg: ""}, http.StatusCreated)
	}
}

// DeleteMerchantByEmail deletes a merchant
func DeleteMerchantByEmail() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		email := chi.URLParam(r, "email")
		if len(strings.Trim(email, " ")) < 1 {
			log.Printf("invalid input email: %s ", email)
			utils.WriteResponseAsJson(w, models.Response{Success: false, Data: nil, ErrorMsg: "invalid input"}, http.StatusBadRequest)
			return
		}
		err := db.MgClient.DeleteMany(db.DB_NAME, db.COL_MERCHANTS, options.Delete(), bson.D{{"email", email}})
		if err != nil {
			log.Printf("error while deleting merchant: %s ", err)
			utils.WriteResponseAsJson(w, models.Response{Success: false, Data: nil, ErrorMsg: err.Error()}, http.StatusInternalServerError)
			return
		}
		utils.WriteResponseAsJson(w, models.Response{Success: true, Data: "deleted", ErrorMsg: ""}, http.StatusOK)
	}
}

// UpdateMerchant updates a merchant
func UpdateMerchant() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		email := chi.URLParam(r, "email")
		if len(strings.Trim(email, " ")) < 1 {
			log.Printf("invalid input email: %s ", email)
			utils.WriteResponseAsJson(w, models.Response{Success: false, Data: nil, ErrorMsg: "invalid input"}, http.StatusBadRequest)
			return
		}

		var merchantAccount models.UpdateMerchantAccount
		err := json.NewDecoder(r.Body).Decode(&merchantAccount)
		if err != nil {
			log.Printf("invalid input email: %s ", err)
			utils.WriteResponseAsJson(w, models.Response{Success: false, Data: nil, ErrorMsg: "invalid input"}, http.StatusBadRequest)
			return
		}
		err = db.MgClient.UpdateMany(db.DB_NAME, db.COL_MERCHANTS, bson.D{{"email", email}}, options.Update(), bson.D{{"$set", bson.D{{"account_name", merchantAccount.AccountName}}}})
		if err != nil {
			log.Printf("error while updating merchant: %s ", err)
			utils.WriteResponseAsJson(w, models.Response{Success: false, Data: nil, ErrorMsg: "something went wrong"}, http.StatusInternalServerError)
			return
		}
		utils.WriteResponseAsJson(w, models.Response{Success: true, Data: "updated", ErrorMsg: ""}, http.StatusOK)
	}
}
