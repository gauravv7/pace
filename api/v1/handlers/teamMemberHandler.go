package handlers

import (
	"encoding/json"
	"github.com/gauravv7/merchantapp/internal/db"
	"github.com/gauravv7/merchantapp/internal/models"
	"github.com/gauravv7/merchantapp/internal/utils"
	"github.com/go-chi/chi"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net/http"
	"strings"
)

func TeamMemberRoutes( /* any dependency injection comes here*/ ) *chi.Mux {
	r := chi.NewRouter()

	r.Get("/all", GetAllTeamMembers())              //Implementation of routes from merchantHandler.go
	r.Get("/email/{email}", GetAllTeamMembersByEmail()) //Implementation of routes from merchantHandler.go
	r.Post("/create", CreateTeamMember())
	r.Delete("/email/{email}", DeleteTeamMemberByEmail())
	r.Put("/email/{email}", UpdateTeamMember())

	return r
}


//GetAllTeamMembers get all team-members
func GetAllTeamMembers() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		curPage := r.URL.Query().Get("page")
		if strings.Trim(curPage, " ") == "" {
			curPage = "1"
		}

		count := r.URL.Query().Get("count")
		if strings.Trim(count, " ") == "" {
			count = "10"
		}


		teamMembers := make([]models.TeamMember, 1)
		selector := []bson.M{
			{"$lookup": bson.M{"from": db.COL_MERCHANTS, "localField": "merchant_account_number","foreignField": "account_number", "as": "merchant"}},
			{"$unwind": "$merchant"},
			{"$skip": (utils.GetInt64(curPage)-1)*utils.GetInt64(count)},
			{"$limit": utils.GetInt64(count)},
		}
		err := db.MgClient.Aggregate(db.DB_NAME, db.COL_TEAM_MEMBERS, selector, options.Aggregate(), &teamMembers)
		if err != nil {
			log.Println("error while fetching data", err)
			utils.WriteResponseAsJson(w, models.Response{Success: false, Data: nil, ErrorMsg: err.Error()}, http.StatusInternalServerError)
			return
		}
		utils.WriteResponseAsJson(w, teamMembers, http.StatusOK)
	}
}


//GetAllTeamMembersByEmail get all team-members by email
func GetAllTeamMembersByEmail() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		teamMembers := make([]models.TeamMember, 1)

		email := chi.URLParam(r, "email")
		if len(strings.Trim(email, " ")) < 1 {
			log.Printf("invalid input email: %s ", email)
			utils.WriteResponseAsJson(w, models.Response{Success: false, Data: nil, ErrorMsg: "invalid input"}, http.StatusInternalServerError)
			return
		}
		selector := []bson.M{
			{"$match": bson.M{"email": email}},
			{"$lookup": bson.M{"from": db.COL_MERCHANTS, "localField": "merchant_account_number","foreignField": "account_number", "as": "merchant"}},
			{"$unwind": "$merchant"},
		}
		err := db.MgClient.Aggregate(db.DB_NAME, db.COL_TEAM_MEMBERS, selector, options.Aggregate(), &teamMembers)
		if err != nil {
			log.Println("error while fetching data", err)
			utils.WriteResponseAsJson(w, models.Response{Success: false, Data: nil, ErrorMsg: err.Error()}, http.StatusInternalServerError)
			return
		}
		utils.WriteResponseAsJson(w, teamMembers, http.StatusOK)
	}
}



//CreateTeamMember create a team-member
func CreateTeamMember() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var teamMember models.TeamMemberRequest

		// Try to decode the request body into the struct. If there is an error,
		// respond to the client with the error message and a 400 status code.
		err := json.NewDecoder(r.Body).Decode(&teamMember)
		if err != nil {
			log.Printf("error while parsing request: %s ", err)
			utils.WriteResponseAsJson(w, models.Response{Success: false, Data: nil, ErrorMsg: "bad request"}, http.StatusBadRequest)
			return
		}

		teamMember.ID = primitive.NewObjectID()
		err = db.MgClient.InsertOne(db.DB_NAME, db.COL_TEAM_MEMBERS, nil, teamMember)
		if err != nil {
			log.Printf("error while inserting merchant: %s ", err)
			utils.WriteResponseAsJson(w, models.Response{Success: false, Data: nil, ErrorMsg: err.Error()}, http.StatusInternalServerError)
			return
		}
		utils.WriteResponseAsJson(w, models.Response{Success: true, Data: "created", ErrorMsg: ""}, http.StatusCreated)
	}
}


// DeleteTeamMemberByEmail deletes a team-member
func DeleteTeamMemberByEmail() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		email := chi.URLParam(r, "email")
		if len(strings.Trim(email, " ")) < 1 {
			log.Printf("invalid input email: %s ", email)
			utils.WriteResponseAsJson(w, models.Response{Success: false, Data: nil, ErrorMsg: "invalid input"}, http.StatusBadRequest)
			return
		}
		err := db.MgClient.DeleteMany(db.DB_NAME, db.COL_TEAM_MEMBERS, options.Delete(), bson.D{{"email", email}})
		if err != nil {
			log.Printf("error while deleting merchant: %s ", err)
			utils.WriteResponseAsJson(w, models.Response{Success: false, Data: nil, ErrorMsg: err.Error()}, http.StatusInternalServerError)
			return
		}
		utils.WriteResponseAsJson(w, models.Response{Success: true, Data: "deleted", ErrorMsg: ""}, http.StatusOK)
	}
}



// UpdateTeamMember updates a team-member
func UpdateTeamMember() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		email := chi.URLParam(r, "email")
		if len(strings.Trim(email, " ")) < 1 {
			log.Printf("invalid input email: %s ", email)
			utils.WriteResponseAsJson(w, models.Response{Success: false, Data: nil, ErrorMsg: "invalid input"}, http.StatusBadRequest)
			return
		}

		var teamMember models.UpdateTeamMember
		err := json.NewDecoder(r.Body).Decode(&teamMember)
		if err != nil {
			log.Printf("invalid input email: %s ", err)
			utils.WriteResponseAsJson(w, models.Response{Success: false, Data: nil, ErrorMsg: "invalid input"}, http.StatusBadRequest)
			return
		}
		err = db.MgClient.UpdateMany(db.DB_NAME, db.COL_TEAM_MEMBERS, bson.D{{"email", email}}, options.Update(), bson.D{{"$set", bson.D{{"name", teamMember.Name}}}})
		if err != nil {
			log.Printf("error while updating merchant: %s ", err)
			utils.WriteResponseAsJson(w, models.Response{Success: false, Data: nil, ErrorMsg: "something went wrong"}, http.StatusInternalServerError)
			return
		}
		utils.WriteResponseAsJson(w, models.Response{Success: true, Data: "updated", ErrorMsg: ""}, http.StatusOK)
	}
}
